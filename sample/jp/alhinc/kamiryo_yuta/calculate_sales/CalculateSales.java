package jp.alhinc.kamiryo_yuta.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		Map<String, String> nameMap = new HashMap<String, String>();
		Map<String, Long> salesMap = new HashMap<String, Long>();
		//System.out.println("ここにあるファイルを開きます =>" + args[0]);

		//
		BufferedReader br = null;
		//メソッド呼び出し
		if(! readLstFile(args[0], "branch.lst", nameMap, salesMap,"[0-9]{3}", "支店")) {
			return;
		}

		File dir = new File(args[0]);

		File[] salesList = dir.listFiles();

		List<String> firstList = new ArrayList<String>();

		for(int i = 0; i < salesList.length; ++i) {
			if(salesList[i].getName().matches("[0-9]{8}.rcd") && salesList[i].isFile())  {

				//System.out.println(salesList[i].getName());

				firstList.add(salesList[i].getName());
			}
		}
		for(int i = 1; i < firstList.size(); ++i) {

			String before = firstList.get(i - 1).substring(0, 8);
			String after = firstList.get(i).substring(0, 8);

			int beforeValue =Integer.valueOf(before);
			int afterValue = Integer.valueOf(after);

			if((afterValue - beforeValue ) != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		for(int i = 0; i < firstList.size(); ++i){

			List<String> secondList = new ArrayList<String>();

			try {
				File file2 = new File(args[0],firstList.get(i));
				FileReader fr = new FileReader(file2);

				br = new BufferedReader(fr);

				String salesLine;

				while((salesLine = br.readLine()) != null) {

					secondList.add(salesLine);

					//System.out.println(secondList);
					//System.out.println(storeMap.get("001"));
				}
				if(secondList.size() != 2){
					System.out.println(file2.getName() + "のフォーマットが不正です");
					return;
				}
				if(!secondList.get(1).matches("[0-9]{1,}")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				if(!(salesMap.containsKey(secondList.get(0)))){
					System.out.println(file2.getName() + "の支店コードが不正です");
					return;
				}


				Long lon1 = salesMap.get(secondList.get(0));
				Long lon2 = Long.parseLong(secondList.get(1));

				//System.out.println(lon1 + lon2);

				Long sum = (lon1 + lon2);

				//System.out.println(sum);

				String digit = String.valueOf(sum);
				if(digit.length() > 10){
					System.out.println("合計金額が10桁を超えました");

					return;
				}
				salesMap.put(secondList.get(0), sum);
				//System.out.println(salesMap);

				//Salesmap.put(secondList.get(0),sum);
				//System.out.println(salesMap);
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try{
						br.close();
					}catch(IOException e){
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		BufferedWriter bw = null;
		//メソッド呼び出し
		if(! readOutFile(args[0], "branch.out", nameMap, salesMap)) {
			return;
		}

	}

	public static boolean readOutFile(String path, String fileName, Map<String, String> nameMap, Map<String, Long> salesMap) {

		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw =  new BufferedWriter(fw);

			for (Map.Entry<String, String> entry : nameMap.entrySet()) {
				String code = entry.getKey();
				String code2 = entry.getValue();

				//System.out.println(code + "," + code2 + "," + salesMap.get(code) + "\n");

				bw.write(code + "," + code2 + "," + salesMap.get(code));
				bw.newLine();


			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try{
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	public static boolean readLstFile(String path, String fileName, Map<String, String> nameMap, Map<String, Long> salesMap,String condition, String storeCode) {

		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			if(! file.exists() ) {
				System.out.println(storeCode + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String defineLine;

			while((defineLine = br.readLine()) != null) {

				String[] branch = defineLine.split(",");

				if(!branch[0].matches(condition)) {
					System.out.println(storeCode + "定義ファイルのフォーマットが不正です");
					return false;
				}
				//System.out.println(branch[0] + branch[1]);

				if(branch.length != 2){
					System.out.println(storeCode + "定義ファイルのフォーマットが不正です");
					return false;
				}

				nameMap.put(branch[0], branch[1]);
				salesMap.put(branch[0], 0L);
			}//System.out.println(storeMap.get("001"));

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}

